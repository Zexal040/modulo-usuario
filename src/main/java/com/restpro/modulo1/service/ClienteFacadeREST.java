/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.restpro.modulo1.service;

import com.restpro.modulo1.entities.Cliente;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Zexal_000
 */
@Stateless
@Path("cliente")
public class ClienteFacadeREST extends AbstractFacade<Cliente> {

    //@PersistenceContext(unitName = "com.RestPro_Modulo1_war_1.0PU")
    private EntityManager em;
    private String JSON_RESPONSE = "{\"Id\":%d, \"Operación\":\"%s\", \"Resultado\":\"%s\"}";

    public ClienteFacadeREST() {
        super(Cliente.class);
    }

    @POST
    @Override
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String create(Cliente entity) {
       String resultado = super.create(entity);
       return String.format(JSON_RESPONSE, entity.getId(), "INSERT", resultado);
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String edit(@PathParam("id") Integer id, Cliente entity) {
        String resultado = super.edit(entity);
        return String.format(JSON_RESPONSE, entity.getId(), "UPDATE", resultado);
    }

    @DELETE
    @Path("{id}")
    public String remove(@PathParam("id") Integer id) {
        String resultado = super.remove(super.find(id));
        String mensaje = "{\"Id\":%d, \"Operación\":\"%s\", \"Resultado\":\"%s\"}";
        return String.format(mensaje, id, "DELETE", resultado);
    }

    @GET
    @Path("{id}")
    @Produces({"application/json"})
    public Cliente find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({"application/json"})
    public List<Cliente> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/json"})
    public List<Cliente> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        if(em == null){
            EntityManagerFactory factory = Persistence.createEntityManagerFactory("com.RestPro_Modulo1_war_1.0PU");
            em = factory.createEntityManager();
        }
        return em;
    }
    
}
